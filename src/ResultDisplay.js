import React from "react";
import axios from "axios";
import {Guild, User} from "./DisplayHelpers";

export class ResultDisplay extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            result: {
                id: this.props.id,
            },
            error: false,
            errorMsg: null,
            complete: false,
            flags: [],
            old_id: null,
        }
        this.beforeRender = this.beforeRender.bind(this);
    }

    componentDidMount() {
        this.beforeRender();
        this.setState({old_id: this.props.id});
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.id !== this.state.old_id) {
            this.beforeRender();
            this.setState({old_id: this.props.id});
        }
    }

    beforeRender() {
        //let url = process.env.API_BASE.concat(
        let url = "https://discard.cc/api/".concat(
            this.props.type === "user" ? "users" : "guilds",
            "/",
            this.props.id
        );
        axios(url)
            .then(r => {
                if(!r.data.avatar) {
                    r.data.avatar = "https://cdn.discordapp.com/embed/avatars/" + r.data.discriminator % 5 + ".png";
                    return r;
                }
                r.data.avatar = "https://cdn.discordapp.com/avatars/" + r.data.id + "/" + r.data.avatar + r.data.avatar.slice(0,2) === "a_" ? ".gif" : ".png";
                return r;
            })
            .then(r => {
                if(!r.data.icon)
                    return r;
                r.data.icon = "https://cdn.discordapp.com/icons/" + r.data.id + "/" + r.data.icon + r.data.icon.slice(0,2) === "a_" ? ".gif" : ".png";
                return r;
            })
            .then(r => this.setState({result: r.data, complete: true}))
            .catch(e => this.setState({
                error: true,
                errorMsg: e.message
            }));
    }



    // eslint-disable-next-line react/require-render-return
    render() {
        return (
            <div id="results">
                <div hidden={this.state.error || !this.state.complete} className="card">
                    {this.props.type === "guild" && <Guild guild={this.state.result}/>}
                    {this.props.type === "user" && <User user={this.state.result}/>}
                </div>
                <p hidden={!this.state.error} style={{color: 'red'}}>{this.state.errorMsg}</p>
            </div>
        );
    }
}