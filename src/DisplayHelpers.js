/* global BigInt */
import {Link} from "@reach/router";

const getFeatures = (features) => {
    let featureList = {
        INVITE_SPLASH: 1n << 0n,
        VIP_REGIONS: 1n << 1n,
        VANITY_URL: 1n << 2n,
        VERIFIED: 1n << 3n,
        PARTNERED: 1n << 4n,
        PUBLIC: 1n << 5n,
        COMMERCE: 1n << 6n,
        NEWS: 1n << 7n,
        DISCOVERABLE: 1n << 8n,
        FEATURABLE: 1n << 9n,
        ANIMATED_ICON: 1n << 10n,
        BANNER: 1n << 11n,
        PUBLIC_DISABLED: 1n << 12n,
        WELCOME_SCREEN_ENABLED: 1n << 13n,
        MEMBER_VERIFICATION_GATE_ENABLED: 1n << 14n,
        ENABLED_DISCOVERABLE_BEFORE: 1n << 15n,
        COMMUNITY: 1n << 16n,
        PREVIEW_ENABLED: 1n << 17n,
        MEMBER_LIST_DISABLED: 1n << 18n,
        MORE_EMOJI: 1n << 19n,
        RELAY_ENABLED: 1n << 20n,
        DISCOVERABLE_DISABLED: 1n << 21n,
    };
    let featureArray = [];
    if(!features)
        return [];
    for(let key in featureList) {
        if((BigInt(features) & featureList[key]) === featureList[key])
            featureArray.push(key);
    }
    return featureArray;
}

const getFlags = (flags) => {
    let flagsList = {
        "STAFF":            1 << 0 ,
        "PARTNER":          1 << 1 ,
        "HYPESQUAD":        1 << 2 ,
        "BUG_HUNTER_LV1":   1 << 3 ,
        "BRAVERY":          1 << 6 , // hypesquad
        "BRILLIANCE":       1 << 7 , // hypesquad
        "BALANCE":          1 << 8 , // hypesquad
        "EARLY_SUPPORTER":  1 << 9 ,
        "TEAM_USER":        1 << 10, // ????
        "SYSTEM":           1 << 12, // reserved for the Discord#0000 user
        "BUG_HUNTER_LV2":   1 << 14,
        "VERIFIED_BOT":     1 << 16,
        "VERIFIED_DEV":     1 << 17,
    };
    let userFlags = [];
    //let emojis = [];
    for (let key in flagsList) {
        // eslint-disable-next-line
        if ((flags & flagsList[key]) === flagsList[key]) {
            userFlags.push(key);
        }
    }
    // TODO: make it do emoji stuff (yeah!)
    return userFlags;
}

export const Guild = ({guild}) => {
    return (
        <div id="info-guild">
            <img src={guild.icon} width="64" height="64" alt="Guild icon"/>
            <p>Name: {guild.name || "Unknown"} ({guild.id || "None"})</p>
            <p>Description: {guild.description || "None"}</p>
            <p>Member Count (maximum): {guild.member_count} ({guild.max_members})</p>
            <p>Owner's ID:
                <Link to={guild.owner_id}>
                    {guild.owner_id}
                </Link>
            </p>
            <p>Region: {guild.region}</p>
            <p>Preferred Locale (language): {guild.preferred_locale}</p>
            <p>Number of boosts: {guild.premium_subscription_count}</p>
            <p>Features: {getFeatures(guild.features).join(", ")}</p>
        </div>
    );
}

export const User = ({user}) => {
    return (
        <div id="info-user">
            <img src={user.avatar} width="64" height="64" alt="Guild icon"/>
            <p>Name: {user.username + "#" + user.discriminator || "Unknown#0000"} ({user.id || "None"})</p>
            <p>Bot: {user.bot ? "Yes" : "No"}</p>
            <p>Is Deleted: {user.deleted ? "Yes" : "No"}</p>
            <p>Flags: {getFlags(user.flags).join(", ") || "None"}</p>
            <p>Nitro Subscriber Status: {(user.premium_guild_since && user.premium_since && "Nitro") || (user.premium_since && "Classic") || "N/A"}</p>
        </div>
    );
}