import './App.css';
import SearchBox from './SearchBox';
import {Link, Router} from "@reach/router";
import {ResultDisplay} from "./ResultDisplay";

function App() {
  return (
    <div className="App">
        Search for... <Link to="users">a user</Link> or <Link to="guilds">a guild</Link>
        <Router>
            <SearchBox path="users" type="user">
                <ResultDisplay path=":id" type="user"/>
            </SearchBox>
            <SearchBox path="guilds" type="guild">
                <ResultDisplay path=":id" type="guild"/>
            </SearchBox>
        </Router>
    </div>
  );
}

export default App;
