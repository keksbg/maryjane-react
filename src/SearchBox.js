import React from "react";
import axios from "axios";
import {Link} from "@reach/router";

const source = axios.CancelToken.source();

export default class SearchBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentInput: '',
            autoSearchResults: [],
            selected: null,
            error: false,
            errormsg: '',
        };
    }

    updateInput(e) {
        let value = e.target.value;
        this.setState({currentInput: value});
        if(value.length >= 3) {
            let url = "https://discard.cc/api/search/".concat(this.props.type === "user" ? "users" : "guilds");
            //source.cancel();
            axios.get(url, {
                params: {
                    q: value
                },
                cancelToken: source.token,
            }).then(r => this.setState({autoSearchResults: r.data.results})).catch(r => {
                this.setState({error: true, errormsg: r.message});
            });
        }
    }

    render() {
        return (
            <div id="components">
                <div id="searchField">
                    <input
                        value={this.state.currentInput}
                        placeholder={"Search for a " + this.props.type || "guild or user"}
                        onInput={e => this.updateInput(e)}
                        list="suggestions"
                        style={{width: "256px", height: "32px", marginTop: "16px"}}
                    />
                    <ul>
                        {
                            // e for entry
                            // eslint-disable-next-line array-callback-return
                            this.state.autoSearchResults.map((e, i) => {
                                if(i <= 3) {
                                    return (
                                        <li key={e.id}>
                                            <Link
                                                key={e.id}
                                                to={e.id}
                                                onClick={() => this.setState({autoSearchResults: []})}
                                            >
                                                {e.tag || e.name}
                                            </Link>
                                        </li>
                                    );
                                }
                            })
                        }
                    </ul>
                    <p hidden={!this.state.error} style={{'color': 'red'}}>{this.state.errormsg}</p>
                </div>
                {this.props.children}
            </div>
        )
    }
}